package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class MainActivity extends LogActivity {
    private static final String DEBUG_TAG = "LogsMainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupUI();

    }


    private void setupUI() {
    Button bNext_Activity = findViewById(R.id.bNext_Activity);

    bNext_Activity.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivity(new Intent(MainActivity.this, NextActivity.class));
        }
    });
}


    public void launchNextActivity2(View view) {
        startActivity(new Intent(this, NextActivity2.class));
    }

}